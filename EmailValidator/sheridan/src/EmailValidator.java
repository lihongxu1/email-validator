package src;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidator {
	private static final String regex = "^[a-zA-Z][A-Za-z0-9_!#$%&'*+/=?`{|}~^.-]+@[A-Za-z0-9_!#$%&'*+/=?`{|}~^.-]+.[A-Za-z_!#$%&'*+/=?`{|}~^-]$";

	final static int extMin = 2;
	final static int domainMin = 3;
	final static int accountMin = 3;

	public static boolean isValidEmail(String str) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(str);

		if (matcher.matches() && splitEmailChk(str)) {
			return true;
		}

		else
			return false;
	}

	public static boolean checkAccount(String str) {
		char ch;
		int lowerCase = 0;

		for (int i = 0; i < str.length(); i++) {
			ch = str.charAt(i);

			if (Character.isLowerCase(ch)) {
				lowerCase++;
			}

		}
		return lowerCase >= accountMin;
	}

	public static boolean checkDomain(String str) {
		char ch;
		int lowerCase = 0;
		int numbers = 0;

		for (int i = 0; i < str.length(); i++) {
			ch = str.charAt(i);

			if (Character.isLowerCase(ch)) {
				lowerCase++;
			} else if (Character.isDigit(ch)) {
				numbers++;
			}

		}
		return (numbers + lowerCase) >= domainMin;
	}

	public static boolean checkExt(String str) {
		char ch;
		int lowerCase = 0;
		int upperCase = 0;

		for (int i = 0; i < str.length(); i++) {
			ch = str.charAt(i);

			if (Character.isLowerCase(ch)) {
				lowerCase++;
			} else if (Character.isUpperCase(ch)) {
				upperCase++;
			}

		}
		return (upperCase + lowerCase) >= extMin;
	}

	private static boolean splitEmailChk(String str) {

		String[] parts = str.split("@");
		String account = parts[0];
		String domainExt = parts[1];

		String[] subParts = domainExt.split("\\.");
		String ext = subParts[subParts.length - 1];

		String[] domainCopy = Arrays.copyOf(subParts, subParts.length - 1);

		String domain = String.join("", domainCopy);

		if (checkExt(ext) && checkDomain(domain) && checkAccount(account)) {
			return true;
		} else
			return false;
	}
	public static void main(String[] args) {
		
	}
	
}