package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class EmailValidatorTest {

	@Test
	public void EmailValid( ) {
		boolean isValid = EmailValidator.isValidEmail("jamesli123@gmail.com");
		assertTrue("Unable to Validate Email", isValid);
	}

	@Test
	public void EmailNotValid( ) {
		boolean isValid = EmailValidator.isValidEmail("jamesligmail.com");
		assertFalse("Unable to Validate Email", isValid);
	}
	@Test
	public void EmailBoundaryIn( ) {
		boolean isValid = EmailValidator.isValidEmail("jli@li3.ca");
		assertTrue("Unable to Validate Email", isValid);
	}
	
	@Test
	public void EmailBoundaryOut( ) {
		boolean isValid = EmailValidator.isValidEmail("1jli@li3.ca");
		assertFalse("Unable to Validate Email", isValid);
	}	
	@Test
	public void EmailBoundaryOut2( ) {
		boolean isValid = EmailValidator.isValidEmail("jlili3.ca");
		assertFalse("Unable to Validate Email", isValid);
	}	
	@Test
	public void EmailBoundaryOut3( ) {
		boolean isValid = EmailValidator.isValidEmail("jli@@li3.ca");
		assertFalse("Unable to Validate Email", isValid);
	}	
	
	@Test
	public void AccountValid( ) {
		boolean isValid = EmailValidator.checkAccount("bob2342&_!!li");
		assertTrue("Unable to Validate Account Name", isValid);
	}
	
	@Test
	public void AccountInValid( ) {
		boolean isValid = EmailValidator.checkAccount("B2342&_!!li");
		assertFalse("Unable to Validate Account Name", isValid);
	}	
	
	@Test
	public void AccountInValidBIn( ) {
		boolean isValid = EmailValidator.checkAccount("abcB2342&_!!");
		assertTrue("Unable to Validate Account Name", isValid);
	}	
	
	@Test
	public void DomainValid( ) {
		boolean isValid = EmailValidator.checkDomain("ggg124c34");
		assertTrue("Unable to Validate Domain Name", isValid);
	}	
	
	@Test
	public void DomainInValid( ) {
		boolean isValid = EmailValidator.checkDomain("gG1");
		assertFalse("Unable to Validate Domain Name", isValid);
	}
	@Test
	public void DomainValidBIn( ) {
		boolean isValid = EmailValidator.checkDomain("gg1");
		assertTrue("Unable to Validate Domain Name", isValid);
	}
	@Test
	public void DomainInValidBOut( ) {
		boolean isValid = EmailValidator.checkDomain("g1");
		assertFalse("Unable to Validate Domain Name", isValid);
	}
	
	@Test
	public void ExtValid( ) {
		boolean isValid = EmailValidator.checkExt("ca");
		assertTrue("Unable to Validate Ext Name", isValid);
	}
	
	@Test
	public void ExtInValid( ) {
		boolean isValid = EmailValidator.checkExt("C");
		assertFalse("Unable to Validate Ext Name", isValid);
	}
	
}
